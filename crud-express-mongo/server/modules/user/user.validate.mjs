import Joi from 'joi';



export const userValidate = Joi.object({
    first_name: Joi.string().pattern(/^[a-zA-z]+$/).min(3).max(30).required(),
    last_name: Joi.string().pattern(/^[a-zA-Z]+$/).min(3).max(30).required(),
    email: Joi.string().email(),
    phone: Joi.string().pattern(/^[0-9]+$/).min(7).max(10).required()
});
  