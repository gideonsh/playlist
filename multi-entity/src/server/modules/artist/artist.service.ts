import log from "@ajar/marker";
import * as artist_action from "./mongo_service/artist.actions.js";
//import * as artist_action from "./mysql_service/artist.actions.js";


export const get_all_artists = async () => {
  const artists = await artist_action.get_all_artists();
  return artists;
  
};
export const create_artist = async (payload : any) => {
  const artist = await artist_action.create_artist(payload);
  return artist;
};
export const get_artist_by_id = async (artist_id : string) => {
  const artist = await artist_action.get_artist_by_id(artist_id);
  return artist;
};
export const update_artist_by_id = async (artist_id : string, payload : any) => {
  const artist = await artist_action.update_artist_by_id(artist_id, payload);
  return artist;
};
export const delete_artist_by_id = async (artist_id : string) => {
    const artist = await artist_action.delete_artist_by_id(artist_id);
    return artist;
};

