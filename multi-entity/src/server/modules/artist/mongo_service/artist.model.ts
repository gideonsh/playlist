import mongoose from "mongoose";
const { Schema, model } = mongoose;
//import {SongSchema} from "../song/song.model.js";

const ArtistSchema = new Schema({
    name   : {type : String, required : true},
    creator: {type : String, required : true},
    songs       : [{type: Schema.Types.ObjectId, ref:"song"}],
}, {timestamps:true});
  
export default model("artist",ArtistSchema);


// import mongoose from 'mongoose'
// const { Schema, model } = mongoose
// import {StorySchema} from '../story/story.model.mjs'
// // import { MeetingSchema } from '../meeting/meeting.model.mjs'

// export const UserSchema = new Schema({
//     first_name  : { type : String, required : true },
//     last_name   : { type : String, required : true },
//     email       : { type : String, required : true },
//     phone       : { type : String },
//     stories     : [ StorySchema ],
//     // meetings    : [ MeetingSchema ],
//     // stories     : [ { type: Schema.Types.ObjectId, ref:'story'} ],
//     meetings    : [ { type: Schema.Types.ObjectId, ref:'meeting'} ],
// }, {timestamps:true});
  
// export default model('user',UserSchema);