import log from "@ajar/marker";
import artist_model from "./artist.model.js";

export const get_all_artists = async () => {
  const artists = await artist_model.find();
  return artists;
};
export const create_artist = async (payload : any) => {
  const artist = await artist_model.create(payload);
  return artist;
};
export const get_artist_by_id = async (artist_id : string) => {
  const artist = await artist_model.findById(artist_id);//.populate("song");
  return artist;
};
export const update_artist_by_id = async (artist_id : string, payload : any) => {
  const artist = await artist_model.findByIdAndUpdate(artist_id, payload, {
    new: true,
    upsert: true,
  });//.populate("song");
  return artist;
};
export const delete_artist_by_id = async (artist_id : string) => {
    const artist = await artist_model.findByIdAndRemove(artist_id);
    return artist;
};

