import log from "@ajar/marker";
import * as artist_service from "../../artist/artist.service.js";
import song_model from "./song.model.js";

export const get_all_songs = async () => {
  const songs = await song_model.find();//.populate("artist");
  return songs;
};

export const create_song = async (payload : any) => {
  const song = await song_model.create(payload);
  const newArtist = await artist_service.get_artist_by_id(payload.artist);
  newArtist.songs.push(song._id);
  console.log(newArtist.songs);
  await artist_service.update_artist_by_id(payload.artist, newArtist);

  return song;
};

export const get_song_by_id = async (song_id : string) => {
  const song = await song_model.findById(song_id);//.populate("artist");
  return song;
};

export const update_song_by_id = async (song_id : string, payload : any) => {
  const song = await song_model.findByIdAndUpdate(song_id, payload, {
    new: true,
    upsert: true,
  });//.populate("artist");

  return song;
};
export const delete_song_by_id = async (song_id : string) => {
    const song = await song_model.findByIdAndRemove(song_id);
    return song;
};
