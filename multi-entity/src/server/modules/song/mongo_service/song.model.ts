import mongoose from "mongoose";
const { Schema, model } = mongoose;
//import {ArtistSchema} from "../artist/artist.model.js";

const SongSchema = new Schema({
    song_name  : {type : String, required : true},
    creator    : {type : String, required : true},
    artist     : {type: Schema.Types.ObjectId, ref:"artist"},
}, {timestamps:true});
  
export default model("song",SongSchema);
