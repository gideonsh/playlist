import log from "@ajar/marker";
//import * as song_action from "./mongo_service/song.actions.js";
import * as song_action from "./mysql_service/song.actions.js";


export const get_all_songs = async () => {
  const songs = await song_action.get_all_songs();
  return songs;
};

// export const create_song = async (payload : any) => {
//   const song = await song_action.create_song(payload);
//   return song;
// };

// export const get_song_by_id = async (song_id : string) => {
//   const song = await song_action.get_song_by_id(song_id);
//   return song;
// };

// export const update_song_by_id = async (song_id : string, payload : any) => {
//   const song = await song_action.update_song_by_id(song_id, payload);
//   return song;
// };

// export const delete_song_by_id = async (song_id : string) => {
//     const song = await song_action.delete_song_by_id(song_id);
//     return song;
// };
