import express, { Request, Response, NextFunction } from "express";
import * as song_service from "./song.service.js";
import raw from "./../../middleware/route.async.wrapper.js";
import * as fs from "fs";

const router = express.Router();
router.use(express.json());

function printToLog(req : Request, res : Response, next : NextFunction) {
    const method = req.method;
    const path = req.path;
    const time = Date.now();
    const content = `method: ${method}, path: ${path}, time: ${time} \n`;

    fs.writeFile("./logFile.txt", content, { flag: "a" }, err => {
    if (err) {
        console.error(err);
        return;
    }
    //file written successfully
    });

    next();
};

router.use(printToLog);

// GET ALL SONGS       
router.get("/"    , raw( async (req : Request, res : Response) => { 
  const songs = await song_service.get_all_songs();
  res.status(200).json(songs);
}));

// // CREATES A NEW SONG
// router.post("/"    , raw (async (req : Request, res : Response) => {
//      const song = await song_service.create_song(req.body);
//      res.status(200).json(song);
// }));

// // GETS SONG BY ID
// router.get("/:id" , raw( async (req : Request, res : Response) => {
//   const song = await song_service.get_song_by_id(req.params.id);
//   if (!song) return res.status(404).json({ status: "No song found." });
//   res.status(200).json(song);
// }));

// // UPDATE SONG BY ID
// router.put("/:id" , raw( async (req : Request, res : Response) => {
//   const song = await song_service.update_song_by_id(req.params.id, req.body);
//   res.status(200).json(song);
// }));

// // DELETE SONG BY ID
// router.delete( "/:id" , raw( async (req : Request, res : Response) => {
//   const song = await song_service.delete_song_by_id(req.params.id);
//   if (!song) return res.status(404).json({ status: "No song found." });
//   res.status(200).json(song);
// }));


export default router;
