import * as playlist_action from "./mongo_service/playlist.actions.js";
// import * as playlist_action from "./mysql_service/playlist.actions.js";


export const get_all_playlists = async () => {
  const playlists = await playlist_action.get_all_playlists();
  return playlists;
};
export const create_playlist = async (payload : any) => {
  const playlist = await playlist_action.create_playlist(payload);
  return playlist;
};
export const get_playlist_by_id = async (playlist_id : string) => {
  const playlist = await playlist_action.get_playlist_by_id(playlist_id);
  return playlist;
};
export const update_playlist_by_id = async (playlist_id : string, payload : any) => {
  const playlist = await playlist_action.update_playlist_by_id(playlist_id, payload);
  return playlist;
};
export const delete_playlist_by_id = async (playlist_id : string) => {
    const playlist = await playlist_action.delete_playlist_by_id(playlist_id);
    return playlist;
};