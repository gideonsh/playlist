import log from "@ajar/marker";
import playlist_model from "./playlist.model.js";

export const get_all_playlists = async () => {
  const playlists = await playlist_model.find().populate("song");
  return playlists;
};
export const create_playlist = async (payload : any) => {
  const playlist = await playlist_model.create(payload);
  return playlist;
};
export const get_playlist_by_id = async (playlist_id : string) => {
  const playlist = await playlist_model.findById(playlist_id).populate("song");
  return playlist;
};
export const update_playlist_by_id = async (playlist_id : string, payload : any) => {
  const playlist = await playlist_model.findByIdAndUpdate(playlist_id, payload, {
    new: true,
    upsert: true,
  }).populate("song");
  return playlist;
};
export const delete_playlist_by_id = async (playlist_id : string) => {
    const playlist = await playlist_model.findByIdAndRemove(playlist_id);
    return playlist;
};
