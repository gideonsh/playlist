import mongoose from "mongoose";
const { Schema, model } = mongoose;
//import {SongSchema} from "../song/song.model.js";

const PlaylistSchema = new Schema({
    playlist_name  : {type : String, required : true},
    creator  : {type : String, required : true},
    songs          : [{type: Schema.Types.ObjectId, ref:"song"}],
}, {timestamps:true});
  
export default model("playlist",PlaylistSchema);
