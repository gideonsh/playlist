import log from "@ajar/marker";
import * as artist_service from "../../artist/artist.service.js";
//import user_model from "./user.model.js";
import { sql_con } from "./../../../db/sql/mysql.connection.js";
import { IUser }from "./../../../../types.js";

export const get_all_users = async () => {
    const query = "SELECT * FROM USER";
    const result = await sql_con.query(query, []);
    const match = result[0] as any[];
    const users = match === undefined ? null : match;
    return users;
};


export const create_user = async (payload : any) => {
    const query = "INSERT INTO USER SET ?";
    const result = await sql_con.query(query, [payload]);
    const match = result[0] as any[];
    const users = match === undefined ? null : match;
    return users;
};

export const get_user_by_id = async (user_id : string) => {
    const query = "SELECT * FROM USER WHERE id = ?";
    const result = await sql_con.query(query, [user_id]);
    const match = result[0] as any[];
    const user = match[0] === undefined ? null : match[0];
    return user;
};

export const update_user_by_id = async (user_id : string, payload : Partial<IUser>) => {
    const query = "UPDATE USER SET ? WHERE id = ?";
    await sql_con.query(query, [payload, user_id]);
    const user = await get_user_by_id(user_id);
    return user;
};

export const delete_user_by_id = async (user_id : string) => {
    const query = "DELETE * FROM USER WHERE id = ?";
    const result = await sql_con.query(query, [user_id]);
    const match = result[0] as any[];
    const users = match === undefined ? null : match;
    return users;
};

export const login = async (email : string, password : string) => {
    const query = "SELECT * FROM USER WHERE email = ? and password = ?";
    const result = await sql_con.query(query, [email, password]);
    const match = result[0] as any[];
    const user = match[0] === undefined ? null : match[0];
    return user;
  };