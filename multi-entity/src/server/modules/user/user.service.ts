import log from "@ajar/marker";
//import * as user_action from "./mongo_service/user.actions.js";
import * as user_action from "./mysql_service/user.actions.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

const {CLIENT_ORIGIN, APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION} = process.env;


export const get_all_users = async () => {
  const users = await user_action.get_all_users();
  return users;
};

export const create_user = async (payload : any) => {
  const salt = bcrypt.genSaltSync(10);
  const password = await bcrypt.hash(payload.password , salt);
  const newPayload = {...payload, password};
  const users = await user_action.create_user(newPayload);
  return users;
};

export const get_user_by_id = async (user_id : string) => {
  const users = await user_action.get_user_by_id(user_id);
  return users;
};

export const update_user_by_id = async (user_id : string, payload : any) => {
  const users = await user_action.update_user_by_id(user_id, payload);
  return users;
};

export const delete_user_by_id = async (user_id : string) => {
  const users = await user_action.delete_user_by_id(user_id);
  return users;
};

export async function getAccessToken(refresh_token: string) {
  const decoded = await jwt.verify(refresh_token, APP_SECRET as any) as jwt.JwtPayload;
  const { id } = decoded;

  const user = await user_action.get_user_by_id(id);
  const {role} = user
  const DB_Token = user.token;

  if (DB_Token === refresh_token) {
      const access_token = jwt.sign({ id, role }, APP_SECRET as any, {
          expiresIn: ACCESS_TOKEN_EXPIRATION, //expires in 1 minute
      });
      return access_token;
  }
}

export const login = async (payload : any) => {

  const user = await user_action.get_user_by_id(payload.id);
  const match = await bcrypt.compare(payload.password, user.password);

  if (user && match) {
    // create access token
    const access_token = jwt.sign({ id : user.id, roles : user.roles }, APP_SECRET as string, {
      expiresIn: ACCESS_TOKEN_EXPIRATION // expires in 1 minute for debugging...
    });
    // create refresh token
    const refresh_token = jwt.sign({ id : user.id, roles : user.roles }, APP_SECRET as string, {
        expiresIn: REFRESH_TOKEN_EXPIRATION // expires in 60 days... long-term...
      });
  
    await user_action.update_user_by_id(user.id, { token: refresh_token });
    return { access_token, refresh_token };
  } else {
    throw new Error("invalid inputs");
  }
  

};

export const logout = async (user_id : string) => {
  const user = await user_action.get_user_by_id(user_id);

  return user;
};
