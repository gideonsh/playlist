import express, { Request, Response, NextFunction } from "express";
import * as user_service from "./user.service.js";
import raw from "./../../middleware/route.async.wrapper.js";
import * as fs from "fs";
import cookieParser from "cookie-parser";
import ms from "ms";


const { APP_SECRET, ACCESS_TOKEN_EXPIRATION, REFRESH_TOKEN_EXPIRATION} = process.env;

const router = express.Router();
router.use(express.json());
router.use(cookieParser())

function printToLog(req : Request, res : Response, next : NextFunction) {
    const method = req.method;
    const path = req.path;
    const time = Date.now();
    const content = `method: ${method}, path: ${path}, time: ${time} \n`;

    fs.writeFile("./logFile.txt", content, { flag: "a" }, err => {
    if (err) {
        console.error(err);
        return;
    }
    //file written successfully
    });

    next();
};

router.use(printToLog);


// GET ALL userS       
router.get("/"    , raw( async (req : Request, res : Response) => { 
  const users = await user_service.get_all_users();
  res.status(200).json(users);
}));

// CREATES A NEW user
router.post("/"    , raw (async (req : Request, res : Response) => {
  const users = await user_service.create_user(req.body);
  res.status(200).json(users);
}));

//get-access-token
router.get(
  "/get-access-token",
  raw(async (req, res) => {
      //get refresh_token from client - req.cookies
      const { refresh_token } = req.cookies;

      if (!refresh_token) {
          throw new Error("Unauthorized - Failed to verify refresh_token.");
      }

      const access_token = await user_service.getAccessToken(refresh_token);
      if (access_token) {
          res.status(200).json({ access_token });
      } else {
          throw new Error("Unauthorized - Failed to verify refresh_token.");
      }
  })
);

// USER LOGIN ////////////////////////////////////////////
router.post("/login", raw( async (req : Request, res : Response) => { 
  const { access_token, refresh_token }  = await user_service.login(req.body);

  res.cookie('access_token',access_token, {
    maxAge: ms(ACCESS_TOKEN_EXPIRATION as string), //1 minute
    httpOnly: true
  })
  res.cookie('refresh_token',refresh_token, {
    maxAge: ms(REFRESH_TOKEN_EXPIRATION as string), //60 days
    httpOnly: true
  })

  res.status(200).json(access_token);
}));

// USER LOGOUT ////////////////////////////////////
router.put("/logout", raw( async (req : Request, res : Response) => {
  //const user_id = req.;
  //const user = await user_service.logout(user_id);
  res.clearCookie('access_token');
  res.clearCookie('refresh_token');
  res.status(200).json({status:'You are logged out'})
}));

// GETS user BY ID
router.get("/:id" , raw( async (req : Request, res : Response) => {
  const users = await user_service.get_user_by_id(req.params.id);
  if (!users) return res.status(404).json({ status: "No user found." });
  res.status(200).json(users);
}));

// UPDATE user BY ID
router.put("/:id" , raw( async (req : Request, res : Response) => {
  const users = await user_service.update_user_by_id(req.params.id, req.body);
  res.status(200).json(users);
}));

// DELETE user BY ID
router.delete( "/:id" , raw( async (req : Request, res : Response) => {
  const users = await user_service.delete_user_by_id(req.params.id);
  if (!users) return res.status(404).json({ status: "No user found." });
  res.status(200).json(users);
}));


export default router;
