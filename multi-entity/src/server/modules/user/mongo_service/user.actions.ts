import log from "@ajar/marker";
import * as artist_service from "../../artist/artist.service.js";
import user_model from "./user.model.js";

export const get_all_users = async () => {
  const users = await user_model.find();//.populate("artist");
  return users;
};

export const create_user = async (payload : any) => {
  const user = await user_model.create(payload);
  return user;
};

export const get_user_by_id = async (user_id : string) => {
  const user = await user_model.findById(user_id);//.populate("artist");
  return user;
};

export const update_user_by_id = async (user_id : string, payload : any) => {
  const user = await user_model.findByIdAndUpdate(user_id, payload, {
    new: true,
    upsert: true,
  });//.populate("artist");

  return user;
};

export const delete_user_by_id = async (user_id : string) => {
    const user = await user_model.findByIdAndRemove(user_id);
    return user;
};
