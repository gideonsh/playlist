import mongoose from "mongoose";
const { Schema, model } = mongoose;


const UserSchema = new Schema({
    user_name  : {type : String, required : true},
    password   : {type : String, required : true},
    role       : {type : String, required : true},
    token      : {type : String, required : true},
    playlists  : [{type: Schema.Types.ObjectId, ref:"playlist"}],
}, {timestamps:true});
  
export default model("user",UserSchema);
