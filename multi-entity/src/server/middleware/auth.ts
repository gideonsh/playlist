import { Request, Response, NextFunction } from 'express'
import jwt from "jsonwebtoken";
import raw from "./route.async.wrapper.js";

const { APP_SECRET = "" } = process.env;


export const verifyAuth = raw(
    async (req : Request, res : Response, next : NextFunction) => {
     
    // check header or url parameters or post parameters for token
    const access_token = req.headers['x-access-token'];

    if (!access_token) return res.status(403).json({
        status:'Unauthorized',
        payload: 'No token provided.'
    });

    // verifies secret and checks exp
    const decoded = await jwt.verify(access_token as string, APP_SECRET as any) as jwt.JwtPayload

    // if everything is good, save to request for use in other routes
    req.id = decoded.id ;
    next();
})

