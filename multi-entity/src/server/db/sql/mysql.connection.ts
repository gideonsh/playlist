import mysql from "mysql2/promise";
const { DB_HOST = "localhost", DB_PORT, DB_NAME, DB_USER_NAME, DB_USER_PASSWORD } = process.env;

// export const sql_con = await mysql.createConnection({
//     host: DB_HOST,
//     port:Number(DB_PORT),
//     database: DB_NAME,
//     user: DB_USER_NAME,
//     password:DB_USER_PASSWORD,
//     rowsAsArray: true,
// });

export const sql_con = await mysql.createConnection({
    host: "localhost",
    port:3306,
    database: "music_crud",
    user: "root",
    password:"qwerty",
    rowsAsArray: true,
});