
export declare global {
    
    namespace Express {
        interface Request {
            users: IUser[],
            id:string,
            role:string,
            user:IUser
        }
      }
    }

export type IUser={
    id:string,
    email: string,
    password:string,
    token:string,
    role:string
}

export type ISong={
  id: string,
  song_name: string,
  artist:string,
  status:number,
  creator:string
}

export type IArtist={
  id: string,
  name: string,
  status:number,
  creator:string
}

export type IPlaylist={
  id: string,
  name: string,
  creator:string
}


